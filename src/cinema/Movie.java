package cinema;

public class Movie {
    private String nameEng, nameThai , durationEng,durationThai,description,poster;
    public Movie(String nameEng, String nameThai, String durationThai, String durationEng, String description, String poster){
        this.nameEng = nameEng;
        this.nameThai =nameThai;
        this.durationThai = durationThai;
        this.durationEng = durationEng;
        this.description =description;
        this.poster = poster;
    }

    public String getNameEng() {
        return nameEng;
    }

    public String getNameThai() {
        return nameThai;
    }

    public String getDurationEng() {
        return durationEng;
    }

    public String getDurationThai() {
        return durationThai;
    }

    public String getDescription() {
        return description;
    }

    public String getPoster() {
        return poster;
    }
}
