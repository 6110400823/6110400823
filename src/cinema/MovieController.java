package cinema;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class MovieController  implements Initializable{

    @FXML AnchorPane myMovie;
    @FXML Label hiLabel;

    public static char showtime;

    public static void setShowtime(char showtime) {
        MovieController.showtime = showtime;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        hiLabel.setText("Hi! " + LoginController.username);
    }


    @FXML public void changeScreenMovie1Btn(ActionEvent event) throws IOException {
        setShowtime('1');
        Parent myParent = FXMLLoader.load(getClass().getResource("Showtime.fxml"));
        Scene myScene = new Scene((myParent));

        Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();

        myStage.setScene(myScene);
        myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
        myStage.show();

    }

    @FXML public void changeScreenMovie2Btn(ActionEvent event) throws IOException {
        setShowtime('2');
        Parent myParent = FXMLLoader.load(getClass().getResource("Showtime.fxml"));
        Scene myScene = new Scene((myParent));

        Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();
        myStage.setScene(myScene);
        myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
        myStage.show();

    }

    @FXML public void changeScreenMovie3Btn(ActionEvent event) throws IOException {
        setShowtime('3');
        Parent myParent = FXMLLoader.load(getClass().getResource("Showtime.fxml"));
        Scene myScene = new Scene((myParent));

        Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();

        myStage.setScene(myScene);
        myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
        myStage.show();

    }

    @FXML public void changeScreenMovie4Btn(ActionEvent event) throws IOException {
        setShowtime('4');
        Parent myParent = FXMLLoader.load(getClass().getResource("Showtime.fxml"));
        Scene myScene = new Scene((myParent));

        Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();

        myStage.setScene(myScene);
        myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
        myStage.show();

    }

    @FXML public void logoutBtn(ActionEvent event) throws IOException {
        ControllerTheatre.sitting.clear();
        ControllerTheatre.amount = 0;
        ControllerTheatre.numSeat = 0;
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm");
        //alert.setHeaderText(null);
        alert.setContentText("Confirm to logout program");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            Parent myParent = FXMLLoader.load(getClass().getResource("LoginPage.fxml"));
            Scene myScene = new Scene(myParent);

            Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();
            myStage.setScene(myScene);
            myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
            myStage.show();
        }

    }





}
