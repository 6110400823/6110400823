package cinema;

public class GetRowSeats {
    public static int getPremiumSeat(int theatre){
        if (theatre == 1){
            return 2;
        }
        else if (theatre == 2){
            return 1;
        }
        else if (theatre == 3){
            return 1;
        }
        else if (theatre == 4){
            return 2;
        }
        return 0;

    }
    public static int getNormalSeat(int theatre){
        if (theatre == 1){
            return 3;
        }
        else if (theatre == 2){
            return 4;
        }
        else if (theatre == 3){
            return 3;
        }
        else if (theatre == 4){
            return 3;
        }
        return 0;
    }
    public static int getHoneymoonSeat(int theatre){
        if (theatre == 1){
            return 0;
        }
        else if (theatre == 2){
            return 0;
        }
        else if (theatre == 3){
            return 1;
        }
        else if (theatre == 4){
            return 1;
        }
        return 0;
    }

}
