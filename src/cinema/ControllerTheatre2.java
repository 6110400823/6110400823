package cinema;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

import java.io.IOException;

public class ControllerTheatre2 {
    @FXML VBox seats;
    @FXML Label total, seat1;
    private double amount = 0;
    private int numSeat=0;

    public void initialize(){

        total.setText("Total : "+ getAmount() + " Baht.");
        seat1.setText("Seat : "+numSeat);

        for (int i=0; i<4; i++){
            HBox seat = new HBox();
            for (int j=0; j<10; j++){
                ImageView imageView = new ImageView(new Image("images/normalSeaSt.png"));
                imageView.setFitHeight(73);
                imageView.setFitWidth(65);
                imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        ImageView imageBook = new ImageView(new Image(("images/BookingSeat2.png")));
                        imageBook.setFitHeight(65);
                        imageBook.setFitWidth(65);
                        setAmount(120);
                        numSeat+=1;
                        total.setText("Total : "+ getAmount()+ " Baht.");
                        seat1.setText("Seat : "+numSeat);
                        Object obj = event.getSource();
                        int index = seat.getChildren().indexOf(obj);
                        seat.getChildren().set(index, imageBook);

                        imageBook.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                setAmount(-120);
                                numSeat-=1;
                                total.setText("Total : "+ getAmount() + " Baht.");
                                seat1.setText("Seat : "+numSeat);
                                Object obj = event.getSource();
                                int index = seat.getChildren().indexOf(obj);
                                seat.getChildren().set(index, imageView);
                            }
                        });
                    }
                });
                seat.getChildren().add(imageView);
            }
            seats.getChildren().add(seat);
        }

        for (int i=0; i<1; i++){
            HBox seat = new HBox();
            for (int j=0; j<10; j++){
                ImageView imageView = new ImageView(new Image("images/premiumSeatS.png"));
                imageView.setFitHeight(73);
                imageView.setFitWidth(65);
                imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        ImageView imageBook = new ImageView(new Image(("images/BookingSeat2.png")));
                        imageBook.setFitHeight(65);
                        imageBook.setFitWidth(65);
                        setAmount(220);
                        numSeat+=1;
                        total.setText("Total : "+ getAmount() + " Baht.");
                        seat1.setText("Seat : "+numSeat);
                        Object obj = event.getSource();
                        int index = seat.getChildren().indexOf(obj);
                        seat.getChildren().set(index, imageBook);

                        imageBook.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                setAmount(-220);
                                numSeat-=1;
                                total.setText("Total : "+ getAmount() + " Baht.");
                                seat1.setText("Seat : "+numSeat);
                                Object obj = event.getSource();
                                int index = seat.getChildren().indexOf(obj);
                                seat.getChildren().set(index, imageView);
                            }
                        });
                    }
                });
                seat.getChildren().add(imageView);
            }
            seats.getChildren().add(seat);
        }
    }
    @FXML public void goBackScreenBtn(ActionEvent event) throws IOException {
        Parent myParent = FXMLLoader.load(getClass().getResource("Showtime2.fxml"));
        Scene myScene = new Scene(myParent);

        Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();
        myStage.setScene(myScene);
        myStage.show();
    }

    public void setAmount(double money) {
        this.amount += money;
    }

    public double getAmount() {
        return amount;
    }
}
