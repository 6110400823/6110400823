package cinema;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.text.Text;
import javafx.stage.Stage;


import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

public class Showtime implements Initializable {
    @FXML private Label nameEng, nameThai, timeThai, timeEng, hiLabel, numTheatreEng, numTheatreThai;
    @FXML private Text description;
    @FXML
    private MediaView teaser;
    @FXML private ImageView playImage, poster;
    @FXML private Button timeButton1, timeButton2, timeButton3, timeButton4;

    private MediaPlayer mediaPlayer;
    private boolean isPlay=true;
    private ArrayList<String> title;
    private ArrayList<Movie> movies;





    @FXML public void goBackScreenBtn(ActionEvent event) throws IOException {
        Parent myParent = FXMLLoader.load(getClass().getResource("Movie.fxml"));
        Scene myScene = new Scene((myParent));

        Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();

        myStage.setScene(myScene);
        myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
        myStage.show();
        mediaPlayer.stop();
    }

    @FXML public void goToTheatreBtn(ActionEvent event) throws IOException {
        mediaPlayer.stop();
        ControllerTheatre.theatre = Character.getNumericValue(MovieController.showtime);
        Parent myParent = FXMLLoader.load(getClass().getResource("Theatre.fxml"));
        Scene myScene = new Scene((myParent));

        Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();

        myStage.setScene(myScene);
        myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
        myStage.show();




    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            read();
            readMovie();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int temp = Character.getNumericValue(MovieController.showtime)-1;
        setText1(temp+1);
        //System.out.println(title.get(temp));
        mediaPlayer = new MediaPlayer(new Media(this.getClass().getResource("../Videos/"+title.get(temp)+".mp4").toExternalForm()));
        teaser.setMediaPlayer(mediaPlayer);
        mediaPlayer.play();
            Image image = new Image("images/pause.png");
            playImage.setImage(image);
            playImage.setFitWidth(25);
            playImage.setFitHeight(25);
            playImage.setCursor(Cursor.HAND);
            playImage.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    setPlayImage();
                }
            });

    }

    public void setPlayImage(){
        if (isPlay){
            mediaPlayer.pause();
            setImage();
            isPlay=false;
        }
        else {
            mediaPlayer.play();
            setImage();
            isPlay=true;
        }
    }

    public void setImage(){
        if (isPlay){
            playImage.setImage(new Image("images/playBtn.png"));
            playImage.setFitWidth(25);
            playImage.setFitHeight(25);
            playImage.setCursor(Cursor.HAND);
        }
        else{
            playImage.setImage(new Image("images/pause.png"));
            playImage.setFitWidth(25);
            playImage.setFitHeight(25);
            playImage.setCursor(Cursor.HAND);
        }
    }

    public void read() throws IOException{
        ArrayList<String> title = new ArrayList<>();
        try{
            File file = new File("src/cinema/pathVideo.csv");
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String line = "";
            while ((line = reader.readLine()) != null) {
                title.add(line);
            }
            reader.close();
        }catch (FileNotFoundException exception){
            System.out.println("not found filename");
        }
        this.title = title;
    }

    public void readMovie() throws IOException{
        ArrayList<Movie> movies = new ArrayList<>();
        try{
            File file = new File("src/cinema/Movie.csv");
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String line = "";
            while ((line = reader.readLine()) != null) {
                String[] arrLine;
                arrLine = line.split(",");
                movies.add(new Movie(arrLine[0],arrLine[1],arrLine[2],arrLine[3],arrLine[4],arrLine[5]));
            }
            reader.close();
        }catch (FileNotFoundException exception){
            System.out.println("not found filename");
        }
        this.movies = movies;
    }


    public void setText1(int num){
        int line=1;
        Movie setMovie = null;
        for (Movie movie : movies){
            if (line == num){
                setMovie = movie;
                //System.out.println(movie.getPoster());
                Booking.myMovie = movie;
            }
            line++;
        }
        numTheatreEng.setText("Theatre "+ num);
        numTheatreThai.setText("โรงภาพยนตร์ " +num);
        hiLabel.setText("Hi! "+ LoginController.username);
        poster.setImage(new Image("images/"+setMovie.getPoster()));
        nameEng.setText(setMovie.getNameEng());
        nameThai.setText(setMovie.getNameThai());
        timeThai.setText("เวลา : "+setMovie.getDurationThai());
        timeEng.setText("Time : " + setMovie.getDurationEng());
        description.setText(setMovie.getDescription());
        if (num == 1){
            timeButton1.setText("8:30");
            timeButton2.setText("11:00");
            timeButton3.setText("13:10");
            timeButton4.setText("18:40");
        }
        if (num == 2){
            timeButton1.setText("8:00");
            timeButton2.setText("11:20");
            timeButton3.setText("14:00");
            timeButton4.setText("17:15");
        }
        if (num == 3){
            timeButton1.setText("13:50");
            timeButton2.setText("18:30");
            timeButton3.setText("20:15");
            timeButton4.setText("22:40");
        }
        if (num == 4){
            timeButton1.setText("10:15");
            timeButton2.setText("13:50");
            timeButton3.setText("15:05");
            timeButton4.setText("18:00");
        }
    }

    @FXML public void logoutBtn(ActionEvent event) throws IOException {
        ControllerTheatre.sitting.clear();
        ControllerTheatre.amount = 0;
        ControllerTheatre.numSeat = 0;
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm");
        //alert.setHeaderText(null);
        alert.setContentText("Confirm to logout program");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            Parent myParent = FXMLLoader.load(getClass().getResource("LoginPage.fxml"));
            Scene myScene = new Scene(myParent);

            Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            myStage.setScene(myScene);
            myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
            myStage.show();
        }
    }

}
