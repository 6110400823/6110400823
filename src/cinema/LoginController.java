package cinema;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.awt.*;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginController implements Initializable {

    @FXML
    AnchorPane myLogin;
    @FXML
    TextField usernameText;
    @FXML
    PasswordField passwordText;

    public static AnchorPane myLoginN;
    ArrayList<User> users;
    public static String username;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(!Main.isSplash){
            loadSplash();
        }
        myLoginN = myLogin;
        try {
            users = readUser.readFile("user");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadSplash(){
        try {
            Main.isSplash = true;

            GridPane pane = FXMLLoader.load(getClass().getResource("Splash.fxml"));
            myLogin.getChildren().setAll(pane);

            FadeTransition fadeIn = new FadeTransition(Duration.seconds(3),pane);
            fadeIn.setFromValue(0);
            fadeIn.setToValue(1);
            fadeIn.setCycleCount(1);

            FadeTransition fadeOut = new FadeTransition(Duration.seconds(3),pane);
            fadeOut.setFromValue(0);
            fadeOut.setToValue(0);
            fadeOut.setCycleCount(1);

            fadeIn.play();

            fadeIn.setOnFinished((e) ->{
                fadeOut.play();
            });

            fadeOut.setOnFinished((e) ->{
                try {
                    AnchorPane myContent = FXMLLoader.load(getClass().getResource("LoginPage.fxml"));
                    myLogin.getChildren().setAll(myContent);
                } catch (IOException ex) {
                    Logger.getLogger(MovieController.class.getName()).log(Level.SEVERE, null, ex);
                }

            });

        } catch (Exception ex) {
            Logger.getLogger(MovieController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML public void loginBtn(ActionEvent event) throws Exception{

        boolean isUsername = false;
        boolean isPassword = false;


        try {
            if (!usernameText.getText().isEmpty() && !passwordText.getText().isEmpty()){
                for (User user : users){
                    if (usernameText.getText().equals(user.getUsername())){
                        isUsername = true;
                        username = usernameText.getText();
                        System.out.println(username);

                    }
                    if (passwordText.getText().equals(user.getPassword())){
                        isPassword = true;
                    }
                }
                if (isUsername && isPassword){
                    Parent myParent = FXMLLoader.load(getClass().getResource("Movie.fxml"));
                    Scene myScene = new Scene((myParent));

                    Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();

                    myStage.setScene(myScene);
                    myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
                    myStage.show();
                    clear();

                    //System.out.println(usernameText.getText());
                }
                if ((isPassword && !isUsername) || (!isPassword && !isUsername)){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Invalid Input!");
                    alert.setHeaderText(null);
                    alert.setContentText("The program not have this user, Please try again.");
                    alert.showAndWait();
                    clear();
                }
                if (isUsername && !isPassword){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Invalid Input!");
                    alert.setHeaderText(null);
                    alert.setContentText("Invalid Password, Please try again.");
                    alert.showAndWait();
                    clear();
                }
            }else{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error!");
                alert.setHeaderText(null);
                alert.setContentText("Please enter username and password.");
                alert.showAndWait();
            }


        } catch (NullPointerException exception){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error!");
            alert.setHeaderText(null);
            alert.setContentText("Please enter username and password.");
            alert.showAndWait();
        }

    }

    public void clear(){
        usernameText.setText(null);
        passwordText.setText(null);
    }

    @FXML public void registerBtn(ActionEvent event) throws IOException {
        Parent myParent = FXMLLoader.load(getClass().getResource("Register.fxml"));
        Scene myScene = new Scene((myParent));

        Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();

        myStage.setScene(myScene);
        myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
        myStage.show();
    }

    @FXML public void goToAboutMePage(ActionEvent event) throws IOException {
        Parent myParent = FXMLLoader.load(getClass().getResource("AboutMe.fxml"));
        Scene myScene = new Scene(myParent);

        Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();
        myStage.setScene(myScene);
        myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
        myStage.show();
    }

    @FXML public void backBtn(ActionEvent event) throws Exception{
        Parent myParent = FXMLLoader.load(getClass().getResource("LoginPage.fxml"));
        Scene myScene = new Scene((myParent));

        Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();

        myStage.setScene(myScene);
        myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
        myStage.show();
    }

    @FXML public void openHintButton(ActionEvent event) throws IOException{
        try {
            File file = new File("6110400823.pdf");
            if(!Desktop.isDesktopSupported()) {
                System.out.println("desktop unsupported.");
                return;
            }
            Desktop desktop = Desktop.getDesktop();
            if(file.exists())
                desktop.open(file);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
