package cinema;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.awt.print.Book;
import java.io.*;
import java.util.ArrayList;
import java.util.Optional;

public class ControllerTheatre {
    @FXML private VBox seats;
    @FXML private Label total, seat1;
    @FXML private Label hiLabel, numTheatreEng, numTheatreThai, typeCinema;
    public static double amount = 0;
    public static int  numSeat=0;
    char name = 'A'; int temp;
    public static int theatre;
    public static ArrayList<String> sitting = new ArrayList<>();
    private boolean isBook = false;
    ArrayList<BookingData> bookingData;


    public void initialize() throws IOException {
        readBooking();
        total.setText("Total : "+ getAmount() + " Baht.");
        seat1.setText("Seat : "+numSeat);
        hiLabel.setText("Hi! " +LoginController.username);
        numTheatreEng.setText("Theatre "+ theatre);
        numTheatreThai.setText("โรงภาพยนตร์ "+ theatre);
        if (theatre == 1){
            typeCinema.setText("Digital Cinema1");
        }
        else if (theatre == 2){
            typeCinema.setText("Digital Cinema2");
        }
        else if (theatre == 3){
            typeCinema.setText("4K Digital Cinema");
        }
        else{
            typeCinema.setText("3Dx Cinema");
        }



        for (int i=0; i<GetRowSeats.getNormalSeat(theatre); i++){

            HBox seat = new HBox();
            for (int j=0; j<10; j++){
                ImageView imageView = new ImageView(new Image("images/normalSeaSt.png"));
                imageView.setFitHeight(73);
                imageView.setFitWidth(65);
                int finalJ = j;
                char finalN = name;
                checkBooking(finalN,finalJ+1);
                System.out.println(finalJ+" "+ finalN);
                System.out.println(isBook);
                if (isBook == false){
                    ImageView finalImageView = imageView;
                    imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            ImageView imageBook = new ImageView(new Image(("images/BookingSeat2.png")));
                            imageBook.setFitHeight(65);
                            imageBook.setFitWidth(65);
                            setAmount(120);
                            numSeat+=1;
                            temp= finalJ +1;
                            sitting.add(""+finalN+temp);
                            total.setText("Total : "+ getAmount()+ " Baht.");
                            seat1.setText("Seat : "+numSeat);
                            Object obj = event.getSource();
                            int index = seat.getChildren().indexOf(obj);
                            seat.getChildren().set(index, imageBook);

                            imageBook.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent event) {
                                    setAmount(-120);
                                    numSeat-=1;
                                    sitting.remove(""+finalN+temp);
                                    total.setText("Total : "+ getAmount() + " Baht.");
                                    seat1.setText("Seat : "+numSeat);
                                    Object obj = event.getSource();
                                    int index = seat.getChildren().indexOf(obj);
                                    seat.getChildren().set(index, finalImageView);
                                }
                            });
                        }
                    });
                }
                else{
                    imageView = new ImageView(new Image("images/normalSeaStO.png"));
                    imageView.setFitHeight(73);
                    imageView.setFitWidth(65);

                }

                seat.getChildren().add(imageView);
            }name++;
            seats.getChildren().add(seat);
            isBook = false;
        }

        //isBook = false;
        for (int i=0; i<GetRowSeats.getPremiumSeat(theatre); i++){
            HBox seat = new HBox();
            for (int j=0; j<10; j++){
                ImageView imageView = new ImageView(new Image("images/premiumSeatS.png"));
                imageView.setFitHeight(73);
                imageView.setFitWidth(65);
                int finalJ = j;
                char finalN = name;
                checkBooking(finalN,finalJ+1);
                System.out.println(finalJ+" "+ finalN);
                System.out.println(isBook);
                if (isBook = true) {
                    ImageView finalImageView = imageView;
                    imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            ImageView imageBook = new ImageView(new Image(("images/BookingSeat2.png")));
                            imageBook.setFitHeight(65);
                            imageBook.setFitWidth(65);
                            setAmount(220);
                            numSeat += 1;
                            temp = finalJ + 1;
                            sitting.add("" + finalN + temp);
                            total.setText("Total : " + getAmount() + " Baht.");
                            seat1.setText("Seat : " + numSeat);
                            Object obj = event.getSource();
                            int index = seat.getChildren().indexOf(obj);
                            seat.getChildren().set(index, imageBook);

                            imageBook.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent event) {
                                    setAmount(-220);
                                    numSeat -= 1;
                                    sitting.remove("" + finalN + temp);
                                    total.setText("Total : " + getAmount() + " Baht.");
                                    seat1.setText("Seat : " + numSeat);
                                    Object obj = event.getSource();
                                    int index = seat.getChildren().indexOf(obj);
                                    seat.getChildren().set(index, finalImageView);
                                }
                            });
                        }
                    });
                }
                else{
                    imageView = new ImageView(new Image("images/premiumSeatSO.png"));
                    imageView.setFitHeight(73);
                    imageView.setFitWidth(65);
                }
                seat.getChildren().add(imageView);
            }name++;
            seats.getChildren().add(seat);
        }
        for (int i=0; i<GetRowSeats.getHoneymoonSeat(theatre); i++){
            HBox seat = new HBox();
            for (int j=0; j<5; j++){
                ImageView imageView = new ImageView(new Image("images/HoneymoonSeatS.png"));
                imageView.setFitHeight(73);
                imageView.setFitWidth(65);
                int finalJ = j;
                char finalN = name;
                checkBooking(finalN,finalJ+1);
                System.out.println(finalJ+" "+ finalN);
                System.out.println(isBook);
                imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        ImageView imageBook = new ImageView(new Image(("images/BookingSeat2.png")));
                        imageBook.setFitHeight(65);
                        imageBook.setFitWidth(65);
                        setAmount(690);
                        numSeat+=2;
                        temp= finalJ;
                        sitting.add(""+finalN+(temp*2+1));
                        sitting.add(""+finalN+(temp*2+2));

                        total.setText("Total : "+ getAmount() + " Baht.");
                        seat1.setText("Seat : "+numSeat);
                        Object obj = event.getSource();
                        int index = seat.getChildren().indexOf(obj);
                        seat.getChildren().set(index, imageBook);
                        setMouseClick(imageBook,finalN,seat,imageView);

                    }
                });
                seat.setSpacing(80);
                seat.getChildren().add(imageView);
            }name++;
            seats.getChildren().add(seat);
        }
    }

    @FXML public void goBackScreenBtn(ActionEvent event) throws IOException {
        Parent myParent = FXMLLoader.load(getClass().getResource("Showtime.fxml"));
        Scene myScene = new Scene(myParent);

        Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();
        myStage.setScene(myScene);
        myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
        myStage.show();
    }

    @FXML public void goToConfirmBookingBtn(ActionEvent event) throws IOException{
        Parent myParent = FXMLLoader.load(getClass().getResource("Booking.fxml"));
        Scene myScene = new Scene(myParent);

        Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();
        myStage.setScene(myScene);
        myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
        myStage.show();

    }


    public void setAmount(double money) {
        this.amount += money;
    }

    public double getAmount() {
        return amount;
    }

    public void setMouseClick(ImageView imageBook, int finalN, HBox seat, ImageView imageView){
        imageBook.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setAmount(-690);
                numSeat-=2;
                sitting.remove(""+finalN+(temp*2+1));
                sitting.remove(""+finalN+(temp*2+2));
                total.setText("Total : "+ getAmount() + " Baht.");
                seat1.setText("Seat : "+numSeat);
                Object obj = event.getSource();
                int index = seat.getChildren().indexOf(obj);
                seat.getChildren().set(index, imageView);
            }
        });
    }

    public void readBooking() throws IOException{
        ArrayList<BookingData> bookingData = new ArrayList<>();
        try{
            File file = new File("src/cinema/BookingData.csv");
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String line = "";
            while ((line = reader.readLine()) != null) {
                String[] arrLine;
                arrLine = line.split(",");
                bookingData.add(new BookingData(arrLine[0],arrLine[1],Integer.parseInt(arrLine[2]),Double.parseDouble(arrLine[3]),arrLine[4]));
            }
            reader.close();
        }catch (FileNotFoundException exception){
            System.out.println("not found filename");
        }
        this.bookingData = bookingData;
    }

    public void checkBooking(char name, int num){
        boolean bb = false;
        for (BookingData b : bookingData){
            String[] arrLine;
            arrLine = b.getSitting().split("-");
            //System.out.println(arrLine.length);
            for (String a: arrLine){
                if (a.charAt(0) == name && num == Character.getNumericValue(a.charAt(1))){
                    isBook = true;
                    System.out.println("this : " +a.charAt(0) + " "+ Character.getNumericValue(a.charAt(1)));
                    break;
                }
                else {
                    isBook = false;
                }

//                System.out.println("Other : " +name + " " + num);
//                System.out.println(a);
            }
        }
    }

    @FXML public void logoutBtn(ActionEvent event) throws IOException {
        ControllerTheatre.sitting.clear();
        ControllerTheatre.amount = 0;
        ControllerTheatre.numSeat = 0;
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm");
        //alert.setHeaderText(null);
        alert.setContentText("Confirm to logout program");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            Parent myParent = FXMLLoader.load(getClass().getResource("LoginPage.fxml"));
            Scene myScene = new Scene(myParent);

            Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();
            myStage.setScene(myScene);
            myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
            myStage.show();
        }
    }

}
