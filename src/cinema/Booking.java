package cinema;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

public class Booking  implements Initializable{
    @FXML private Label name,seat,theatre,sitting,total;
    private String nameSeat = "";
    public static Movie myMovie;
    @FXML private ImageView poster;
    private ArrayList<BookingData> bookingData;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            readBookingData();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (String s : ControllerTheatre.sitting){
            nameSeat += s+" ";
        }
        setText();
    }

    @FXML
    public void goBackScreenBtn(ActionEvent event) throws IOException {
        ControllerTheatre.sitting.clear();
        ControllerTheatre.amount = 0;
        ControllerTheatre.numSeat = 0;
        Parent myParent = FXMLLoader.load(getClass().getResource("Theatre.fxml"));
        Scene myScene = new Scene(myParent);

        Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();
        myStage.setScene(myScene);
        myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
        myStage.show();

    }

    public void setText(){
        name.setText(myMovie.getNameEng());
        seat.setText(""+ControllerTheatre.numSeat+" Seat.");
        theatre.setText(""+ControllerTheatre.theatre);
        sitting.setText(nameSeat);
        total.setText(""+ControllerTheatre.amount+" baht.");
        poster.setImage(new Image("images/"+myMovie.getPoster()));
    }

    @FXML public void ConfirmPayBtn(ActionEvent event)throws IOException {
        ControllerTheatre.amount=0;
        ControllerTheatre.numSeat=0;
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm");
        //alert.setHeaderText(null);
        alert.setContentText("Confirm booking and get slip to continue payment at Counter Service.");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            File file = new File("src/cinema/BookingData.csv");
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for(BookingData b : bookingData){
                writer.append(b.getUsername()+","+b.getTitle()+","+b.getNumSeat()+","+b.getPrice()+","+b.getSitting());
                writer.newLine();
            }
            writer.append(LoginController.username+","+myMovie.getNameEng()+","+ControllerTheatre.theatre+","+ControllerTheatre.amount+",");
            int i = 0;
            for (String s : ControllerTheatre.sitting){
                if (i==0){
                    writer.append(s);
                }
                else{
                    writer.append("-"+s);
                }
                i++;
            }
            writer.newLine();
            writer.close();
            ControllerTheatre.amount=0;
            Parent myParent = FXMLLoader.load(getClass().getResource("Movie.fxml"));
            Scene myScene = new Scene((myParent));

            Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();


            myStage.setScene(myScene);
            myStage.show();
        }

    }

    public void readBookingData() throws IOException {
        ArrayList<BookingData> bookingData = new ArrayList<>();
        try{
            File file = new File("src/cinema/BookingData.csv");
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String line = "";
            while ((line = reader.readLine()) != null) {
                String[] arrLine;
                arrLine = line.split(",");
                bookingData.add(new BookingData(arrLine[0], arrLine[1], Integer.parseInt(arrLine[2]), Double.parseDouble(arrLine[3]), arrLine[4]));
            }
            reader.close();
            this.bookingData = bookingData;
        }catch (FileNotFoundException exception){
            System.out.println("not found filename");
        }
    }


}
