package cinema;

import java.util.ArrayList;

public class BookingData{
    private String username, title;
    private int numSeat;
    private double price;
    private String sitting;

    public BookingData(String username, String title, int numSeat, double price, String sitting){
        this.username = username;
        this.title = title;
        this.numSeat = numSeat;
        this.price = price;
        this.sitting = sitting;
    }

    public String getUsername() {
        return username;
    }

    public String getTitle() {
        return title;
    }

    public int getNumSeat() {
        return numSeat;
    }

    public double getPrice() {
        return price;
    }

    public String getSitting() {
        return sitting;
    }
}
