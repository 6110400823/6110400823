package cinema;

import java.io.*;
import java.util.ArrayList;

public class readUser {


    public static ArrayList<User> readFile(String name) throws IOException {
        ArrayList<User> users = new ArrayList<>();

        try{
            File file = new File("csv/"+name+".csv");
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String line = "";
            while ((line = reader.readLine()) != null) {
                String[] arrLine;
                arrLine = line.split(",");
                users.add(new User(arrLine[0], arrLine[1], arrLine[2], arrLine[3], arrLine[4]));
            }
            reader.close();
        }catch (FileNotFoundException exception){
            System.out.println("not found filename");
        }
        return users;
    }
}
