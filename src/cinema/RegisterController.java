package cinema;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;

public class RegisterController{
    @FXML PasswordField passwordText, cfPasswordText;
    @FXML TextField usernameText, emailText, nameText, surnameText;
    private ArrayList<User> users ;


    public void initialize() throws IOException {
        users = readUser.readFile("user");
    }

    @FXML public void submitBtn(ActionEvent event) throws Exception {

        boolean sameUser  =false, sameEmail = false;

        try{
            if (!passwordText.getText().isEmpty() && !cfPasswordText.getText().isEmpty() && !usernameText.getText().isEmpty() && !nameText.getText().isEmpty()
                    && !surnameText.getText().isEmpty() && !emailText.getText().isEmpty()){
                for (User user : users){
                    if (user.getUsername().equals(usernameText.getText())){
                        sameUser = true;
                    }
                    if (user.getEmail().equals(emailText.getText())){
                        sameEmail = true;
                    }
                }

                if (sameEmail && sameUser){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Invalid Input!");
                    alert.setHeaderText(null);
                    alert.setContentText("The email and username have used, Please try again.");
                    alert.showAndWait();
                    clear();

                }
                else if (sameEmail){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Invalid Input!");
                    alert.setHeaderText(null);
                    alert.setContentText("The email has used, Please try again.");
                    alert.showAndWait();
                    clear();

                }
                else if(sameUser){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Invalid Input!");
                    alert.setHeaderText(null);
                    alert.setContentText("The username has used, Please try again.");
                    alert.showAndWait();
                    clear();
                }
                else if(!(passwordText.getText().equals(cfPasswordText.getText()))){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Invalid Input!");
                    alert.setHeaderText(null);
                    alert.setContentText("Password don't match, Please try again.");
                    alert.showAndWait();
                    passwordText.setText(null);
                    cfPasswordText.setText(null);
                }
                else if (passwordText.getText().length() < 4){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Invalid Input!");
                    alert.setHeaderText(null);
                    alert.setContentText("Password must more than 3 characters, Please try again.");
                    alert.showAndWait();
                    passwordText.setText(null);
                    cfPasswordText.setText(null);
                }
                else {
                    File file = new File("src/cinema/user.csv");
                    FileWriter fileWriter = new FileWriter(file);
                    BufferedWriter writer = new BufferedWriter(fileWriter);
                    for (User user : users){
                        //System.out.println(user);
                        writer.append(user.getUsername()+","+user.getPassword()+","+user.getEmail()+","+user.getName()+","+user.getSurname());
                        writer.newLine();
                    }

                    writer.append(usernameText.getText()+","+passwordText.getText()+","+emailText.getText()+","+nameText.getText()+","+surnameText.getText());
                    writer.newLine();
                    writer.close();

                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Thank you for register!!");
                    alert.setHeaderText(null);
                    alert.setContentText("Congratulation, You're member of F Cineplex.");
                    alert.showAndWait();
                    clear();

                    Parent myParent = FXMLLoader.load(getClass().getResource("LoginPage.fxml"));
                    Scene myScene = new Scene((myParent));

                    Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();

                    myStage.setScene(myScene);
                    myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
                    myStage.show();
                }

            }
            else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Invalid Input!");
                alert.setHeaderText(null);
                alert.setContentText("Please, Input data completely");
                alert.showAndWait();
            }
        }catch (NullPointerException exception){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error!");
            alert.setHeaderText(null);
            alert.setContentText("Please, Input data completely.");
            alert.showAndWait();
        }
    }
    @FXML public void backBtn(ActionEvent event) throws Exception{
        Parent myParent = FXMLLoader.load(getClass().getResource("LoginPage.fxml"));
        Scene myScene = new Scene((myParent));

        Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();

        myStage.setScene(myScene);
        myParent.getStylesheets().add(getClass().getResource("/fonts/ControlStyle.css").toExternalForm());
        myStage.show();
    }
    public void clear(){
        usernameText.setText(null);
        passwordText.setText(null);
        emailText.setText(null);
        nameText.setText(null);
        surnameText.setText(null);
        cfPasswordText.setText(null);
    }
}
