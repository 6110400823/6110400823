package cinema;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Showtime2 implements Initializable {
    @FXML private MediaView teaser;
    @FXML private ImageView playImage;
    private MediaPlayer mediaPlayer;
    boolean isPlay=true;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mediaPlayer = new MediaPlayer(new Media(this.getClass().getResource("../Videos/SangKraSue.mp4").toExternalForm()));
        teaser.setMediaPlayer(mediaPlayer);
        mediaPlayer.play();
        Image image = new Image("images/pause.png");
        playImage.setImage(image);
        playImage.setFitWidth(25);
        playImage.setFitHeight(25);
        playImage.setCursor(Cursor.HAND);
        playImage.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setPlayImage();
            }
        });

    }

    @FXML public void goBackScreenBtn(ActionEvent event) throws IOException {
        Parent myParent = FXMLLoader.load(getClass().getResource("Movie.fxml"));
        Scene myScene = new Scene(myParent);

        Stage myStage = (Stage)((Node)event.getSource()).getScene().getWindow();
        myStage.setScene(myScene);
        mediaPlayer.stop();
        myStage.show();

    }

    @FXML public void goToTheatre2Btn(ActionEvent event) throws IOException {
        Button button = (Button) event.getSource();
        Stage stage = (Stage) button.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader((getClass().getResource("Theatre2.fxml")));
        mediaPlayer.stop();
        stage.setScene(new Scene(loader.load()));
    }

    public void setPlayImage(){
        if (isPlay){
            mediaPlayer.pause();
            setImage();
            isPlay=false;
        }
        else {
            mediaPlayer.play();
            setImage();
            isPlay=true;
        }
    }

    public void setImage(){
        if (isPlay){
            playImage.setImage(new Image("images/playBtn.png"));
            playImage.setFitWidth(25);
            playImage.setFitHeight(25);
            playImage.setCursor(Cursor.HAND);
        }
        else{
            playImage.setImage(new Image("images/pause.png"));
            playImage.setFitWidth(25);
            playImage.setFitHeight(25);
            playImage.setCursor(Cursor.HAND);
        }
    }

}
